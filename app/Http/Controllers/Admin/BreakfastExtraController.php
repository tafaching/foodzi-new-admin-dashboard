<?php

namespace App\Http\Controllers\Admin;


use App\BreakfastExtra;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyBreakfastExtraRequest;
use App\Http\Requests\StoreBreakfastExtraRequest;
use App\Http\Requests\UpdateBreakfastExtraRequest;
use App\Shop;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response;

class BreakfastExtraController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {

        $extra = BreakfastExtra::all();

        return view('admin.extra.index', compact('extra'));
    }

    public function create()
    {

        return view('admin.extra.create');
    }

    public function store(StoreBreakfastExtraRequest $request)
    {
        $extra = BreakfastExtra::create($request->all());

        return redirect()->route('extra-index');
    }

    public function edit(BreakfastExtra $extra)
    {

        return view('admin.extra.edit', compact('extra'));
    }

    public function update(UpdateBreakfastExtraRequest $request, BreakfastExtra $extra)
    {
        $extra->update($request->all());
        return redirect()->route('extra-index');
    }

    public function show(BreakfastExtra $extra)
    {

        return view('admin.extra.show', compact('extra'));
    }

    public function destroy(BreakfastExtra $extra)
    {
        $extra->delete();

        return back();
    }

    public function massDestroy(MassDestroyBreakfastExtraRequest $request)
    {
        BreakfastExtra::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
