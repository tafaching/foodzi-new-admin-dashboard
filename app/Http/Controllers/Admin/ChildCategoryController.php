<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Childcategory;
use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyCategoryRequest;
use App\Http\Requests\MassDestroyChildCategoryRequest;
use App\Http\Requests\StoreCategoryRequest;
use App\Http\Requests\StoreChildCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;
use App\Http\Requests\UpdateChildCategoryRequest;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ChildCategoryController extends Controller
{
    public function index()
    {
//        abort_if(Gate::denies('category_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $childcategories = Childcategory::all();

        return view('admin.childCategories.index', compact('childcategories'));
    }

    public function create()
    {
//        abort_if(Gate::denies('category_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.childCategories.create');
    }

    public function store(StoreChildCategoryRequest $request)
    {
        $category = Childcategory::create($request->all());

        return redirect()->route('child-index');
    }

    public function edit(Childcategory $category)
    {
//        abort_if(Gate::denies('category_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.childCategories.edit', compact('category'));
    }

    public function update(UpdateChildCategoryRequest $request, Category $category)
    {
        $category->update($request->all());

        return redirect()->route('child-index');
    }

    public function show(Childcategory $category)
    {
//        abort_if(Gate::denies('category_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.childCategories.show', compact('category'));
    }

    public function destroy(Childcategory $category)
    {
//        abort_if(Gate::denies('category_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $category->delete();

        return back();
    }

    public function massDestroy(MassDestroyChildCategoryRequest $request)
    {
        Childcategory::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
