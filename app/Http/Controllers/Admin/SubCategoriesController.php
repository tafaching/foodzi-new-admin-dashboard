<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyCategoryRequest;
use App\Http\Requests\MassDestroySubCategoryRequest;
use App\Http\Requests\StoreCategoryRequest;
use App\Http\Requests\StoreSubCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;
use App\Http\Requests\UpdateSubCategoryRequest;
use App\Subcategory;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Input;

class SubCategoriesController extends Controller
{


    public function subIndex()
    {
//        abort_if(Gate::denies('category_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $subcategories = Subcategory::all();

        return view('admin.subcategories.index', compact('subcategories'));
    }

    public function subCreate()
    {
//        abort_if(Gate::denies('category_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.subcategories.create');
    }


    public function subStore(StoreSubCategoryRequest $request)
    {
        $category = Subcategory::create($request->all());

        return redirect()->route('sub-index');
    }


    public function subEdit(Subcategory $category)
    {
//        abort_if(Gate::denies('category_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.subcategories.edit', compact('category'));
    }


    public function subUpdate(UpdateSubCategoryRequest $request, Subcategory $category)
    {
        $category->update($request->all());

        return redirect()->route('sub-index');
    }


    public function subShow(Subcategory $category)
    {
//        abort_if(Gate::denies('category_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.subcategories.show', compact('category'));
    }


    public function subDestroy(Subcategory $category)
    {
//        abort_if(Gate::denies('category_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $category->delete();

        return back();
    }


    public function subMassDestroy(MassDestroySubCategoryRequest $request)
    {
        Subcategory::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
