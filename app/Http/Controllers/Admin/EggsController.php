<?php

namespace App\Http\Controllers\Admin;


use App\Eggs;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyBreakfasteggsRequest;
use App\Http\Requests\MassDestroyEggsRequest;
use App\Http\Requests\StoreBreakfasteggsRequest;
use App\Http\Requests\StoreEggsRequest;
use App\Http\Requests\UpdateBreakfasteggsRequest;
use App\Http\Requests\UpdateEggsRequest;
use App\Shop;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response;

class EggsController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {

        $eggs = Eggs::all();

        return view('admin.eggs.index', compact('eggs'));
    }

    public function create()
    {

        return view('admin.eggs.create');
    }

    public function store(StoreEggsRequest $request)
    {
        $eggs = Eggs::create($request->all());

        return redirect()->route('eggs-index');
    }

    public function edit(Eggs $eggs)
    {

        return view('admin.eggs.edit', compact('eggs'));
    }

    public function update(UpdateEggsRequest $request, Eggs $eggs)
    {
        $eggs->update($request->all());
        return redirect()->route('eggs-index');
    }

    public function show(Eggs $eggs)
    {

        return view('admin.eggs.show', compact('eggs'));
    }

    public function destroy(Eggs $eggs)
    {
        $eggs->delete();

        return back();
    }

    public function massDestroy(MassDestroyEggsRequest $request)
    {
        Eggs::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
