<?php

namespace App\Http\Controllers\Admin;

use App\Bacon;
use App\Category;
use App\Day;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyBaconRequest;
use App\Http\Requests\MassDestroyShopRequest;
use App\Http\Requests\StoreBaconRequest;
use App\Http\Requests\UpdateBaconRequest;
use App\Shop;
use App\Http\Controllers\Controller;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig;
use Symfony\Component\HttpFoundation\Response;

class BaconController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {

        $bacon = Bacon::all();

        return view('admin.bacon.index', compact('bacon'));
    }

    public function create()
    {

        return view('admin.bacon.create');
    }

    public function store(StoreBaconRequest $request)
    {
        $shop = Bacon::create($request->all());

        return redirect()->route('bacon-index');
    }

    public function edit(Bacon $bacon)
    {

        return view('admin.bacon.edit', compact('bacon'));
    }

    public function update(UpdateBaconRequest $request, Bacon $bacon)
    {
        $bacon->update($request->all());
        return redirect()->route('bacon-index');
    }

    public function show(Bacon $bacon)
    {

        return view('admin.bacon.show', compact('bacon'));
    }

    public function destroy(Bacon $bacon)
    {
        $bacon->delete();

        return back();
    }

    public function massDestroy(MassDestroyBaconRequest $request)
    {
        Shop::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
