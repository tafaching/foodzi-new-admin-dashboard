<?php

namespace App\Http\Controllers\Admin;


use App\Coffee;
use App\Http\Controllers\Traits\MediaUploadingTrait;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyCoffeeRequest;
use App\Http\Requests\StoreCoffeeRequest;
use App\Http\Requests\UpdateCoffeeRequest;
use Symfony\Component\HttpFoundation\Response;

class CoffeeController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {

        $coffee = Coffee::all();

        return view('admin.coffee.index', compact('coffee'));
    }

    public function create()
    {

        return view('admin.coffee.create');
    }

    public function store(StoreCoffeeRequest $request)
    {
        $coffee = Coffee::create($request->all());

        return redirect()->route('coffee-index');
    }

    public function edit(Coffee $coffee)
    {

        return view('admin.coffee.edit', compact('coffee'));
    }

    public function update(UpdateCoffeeRequest $request, Coffee $coffee)
    {
        $coffee->update($request->all());
        return redirect()->route('coffee-index');
    }

    public function show(Coffee $coffee)
    {

        return view('admin.coffee.show', compact('coffee'));
    }

    public function destroy(Coffee $coffee)
    {
        $coffee->delete();

        return back();
    }

    public function massDestroy(MassDestroyCoffeeRequest $request)
    {
        Coffee::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
