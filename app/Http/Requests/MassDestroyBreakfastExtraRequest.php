<?php

namespace App\Http\Requests;

use App\Shop;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyBreakfastExtraRequest extends FormRequest
{


    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:bacons,id',
        ];
    }
}
