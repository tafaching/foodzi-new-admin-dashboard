<?php

namespace App\Http\Requests;

use App\Shop;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class UpdateBreakfastExtraRequest extends FormRequest
{

    public function rules()
    {
        return [
            'name'         => [
                'required',
            ],
            'description'         => [
                'required',
            ],

        ];
    }
}
