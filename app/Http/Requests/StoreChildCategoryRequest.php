<?php

namespace App\Http\Requests;

use App\Category;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class StoreChildCategoryRequest extends FormRequest
{


    public function rules()
    {
        return [
            'name'         => [
                'required',
            ],
            'description'         => [
                'required',
            ],
            'price'         => [
                'required',
            ],
        ];
    }
}
