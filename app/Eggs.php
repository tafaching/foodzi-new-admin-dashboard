<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Eggs extends Model
{
    use SoftDeletes;

    public $table = 'eggs';


    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'name',
        'description',
        'price',
        'created_at',
        'updated_at',
        'deleted_at',
        'description'
    ];

}
