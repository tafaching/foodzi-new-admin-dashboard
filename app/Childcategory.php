<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Childcategory extends Model
{
    use SoftDeletes;

    public $table = 'childcategories';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'name',
        'description',
        'price',
        'category_id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function shops()
    {
        return $this->belongsToMany(Shop::class);
    }
}
