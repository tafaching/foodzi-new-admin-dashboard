<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subcategory extends Model
{
    use SoftDeletes;

    public $table = 'subcategories';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'name',
        'category_id',
        'description',
        'price',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function shops()
    {
        return $this->belongsToMany(Shop::class);
    }
}
