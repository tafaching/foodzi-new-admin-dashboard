<div class="sidebar" data-color="white" data-active-color="danger">
    <div class="logo">
        <img src="{{asset('img/logo.png')}}">


    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            <li>
                <a href="{{ route('page.index') }}" class="nav-link">
                    <i class="nc-icon nc-bank">

                    </i>
                    {{ trans('cruds.dashboard.title') }}
                </a>
            </li>
            @if(\Illuminate\Support\Facades\Auth::user()->checkAdmin())
                <li>
                    <a href="#menu1" class="accordion-toggle wave-effect" data-toggle="collapse" aria-expanded="false">
                        <i class=""></i>{{ 'User Management' }} <b class="caret"></b>
                    </a>
                    <ul data-toggle="collapse" aria-expanded="false" id="menu1" data-parent="#accordion">
                        <li>
                            <a href="{{ route("admin.users.index") }}"
                               class="nav-link {{ request()->is('admin/users') || request()->is('admin/users/*') ? 'active' : '' }}">

                                {{ trans('cruds.user.title') }}
                            </a>
                        </li>

                    </ul>
                </li>
            @endif

            <li>
                <a href="#menu2" class="accordion-toggle wave-effect" data-toggle="collapse" aria-expanded="false">
                    <i class=""></i>{{ 'Restaurants Management' }} <b class="caret"></b>
                </a>
                <ul data-toggle="collapse" aria-expanded="false" id="menu2" data-parent="#accordion">

                    <li>
                        <a href="{{ route("admin.shops.index") }}"
                           class="nav-link {{ request()->is('admin/shops') || request()->is('admin/shops/*') ? 'active' : '' }}">

                            {{ trans('cruds.shop.title') }}

                        </a>
                    </li>

                </ul>
            </li>


            <li>
                <a href="#menu4" class="accordion-toggle wave-effect" data-toggle="collapse" aria-expanded="false">
                    <i class=""></i>{{ 'Menu Management' }} <b class="caret"></b>
                </a>
                <ul data-toggle="collapse" aria-expanded="false" id="menu4" data-parent="#accordion">

                    <li>
                        <a href="{{ route("admin.categories.index") }}">

                            {{ 'Main Category' }}

                        </a>
                    </li>
                    <li>
                        <a href="{{ route("sub-index") }}">

                            {{ 'Sub Category' }}
                        </a>
                    </li>


                    <li>
                        <a href="{{ route("child-index") }}">

                            {{ 'Child Category' }}

                        </a>
                    </li>

                </ul>
            </li>

            @if(\Illuminate\Support\Facades\Auth::user()->checkManager()||\Illuminate\Support\Facades\Auth::user()->checkStaff())
                <li>
                    <a href="#menu3" class="accordion-toggle wave-effect" data-toggle="collapse" aria-expanded="false">
                        <i class=""></i>{{ 'Staff Management' }} <b class="caret"></b>
                    </a>
                    <ul data-toggle="collapse" aria-expanded="false" id="menu3" data-parent="#accordion">

                        <li>
                            <a href="{{ route("admin.users.index") }}"
                               class="nav-link {{ request()->is('admin/users') || request()->is('admin/users/*') ? 'active' : '' }}">

                                {{ 'Staff' }}
                            </a>
                        </li>

                    </ul>
                </li>
            @endif

        </ul>
    </div>
</div>
