<head>
    <title> @yield('title') | Foodzi </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    
    <link rel="shortcut icon" href="{{asset('img/favico.png')}}"/>
    <!--Global styles -->
    <link type="text/css" rel="stylesheet" href="{{asset('css/components.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('css/custom.css')}}" />
    <!--End of Global styles -->
    <!--Plugin styles-->
    <link type="text/css" rel="stylesheet" href="{{asset('vendors/bootstrapvalidator/css/bootstrapValidator.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('vendors/wow/css/animate.css')}}"/>
    <!--End of Plugin styles-->
    <link type="text/css" rel="stylesheet" href="{{asset('css/pages/login1.css')}}"/>
    <style>
    body, html {
        height: 100% !important;
        background-color: white !important;
    }
    .error{
        color:red;
        width: 100%;
    }
    .login_logo {
        background-color: #495057e6 !important;
    }
      
 .text-success{
        color: #049c14 !important;
    }
    .btn-success{
        background-color: #049c14 !important;
        color: white;
    }
    </style>
</head>