<?php
header("Cache-Control: no-store, must-revalidate, max-age=0");
header("Pragma: no-cache");
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
?>
<!DOCTYPE html>
<html>
    @include('layouts/admin/login/header')
    @yield('header_styles')
<body class="bg">
    @include('messages.message')
    @include('layouts/admin/login/nav')
    @yield('content')
    @include('layouts/admin/login/footer')
    @yield('footer_scripts')
    
</body>
</html>