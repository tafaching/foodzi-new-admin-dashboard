@extends('layouts.admin.login.master')

@section('title')
    Login
@endsection

@section('header_styles')
    <style>
        label {
            display: inline !important;
        }
    </style>
@endsection
@section('content')
    <div class="container">
        <div class="">
            <div class="col-lg-12 col-md-12 col-sm-12 login-panel">
                <div class="row">
                    <div class="col-md-6 offset-3 mt-4">
                        <div class="bg-white login_content login_border_radius shadow p-3 mb-5 bg-white" style="">
                            <div>
                                <img src="{{asset('img/logo.png')}}" alt="logo" style="width: 35%;margin-left: 30%;margin-bottom: 20%;">

                            </div>

                            <form class="form" method="POST" action="{{ route('login') }}">
                                @csrf


                                @if ($message = Session::get('error'))
                                    <div class="text-center">
                                        <label id="login-error" class="error ">{{ $message }}</label>
                                    </div>

                                @endif
                                <br>
                                <div class="form-group col-md-8 offset-2">
                                    <label for="email" class="col-form-label"> E-mail</label>
                                    <div class="input-group input-group-prepend">
                                    <span class="input-group-text  border-right-0  input_email"><i
                                            class="fa fa-envelope text-success"
                                            style="color:#ff6d2c !important;"></i></span>
                                        <input type="text" class="form-control col-md-12" id="email" name="email"
                                               placeholder="E-mail">
                                    </div>
                                </div>
                                <br>
                                <div class="form-group col-md-8 offset-2">
                                    <label for="password" class="col-form-label">Password</label>
                                    <div class="input-group  input-group-prepend ">
                                    <span class="input-group-text  border-right-0  addon_password"><i
                                            class="fa fa-lock text-success"
                                            style="color:#ff6d2c !important;"></i></span>
                                        <input type="password" class="form-control form-control-md" id="password"
                                               name="password" placeholder="Password">
                                    </div>
                                </div>
                                <br>
                                <div class="form-group col-md-8 offset-2">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <input type="submit" value="Log In"
                                                   class="btn btn-success col-md-12 btn-block login_button">
                                        </div>
                                    </div>
                                </div>
                            </form>

                            <div class="form-group col-md-8 offset-2 mb-4">
                                <div class="row">
                                    <div class="col-6" style="display: inline-block;">
                                        <input class="form-check-input mt-1" type="checkbox" name="remember"
                                               id="remember" {{ old('remember') ? 'checked' : '' }}
                                               style="margin-top: 2.8% !important;"
                                        >&nbsp;
                                        <label class="form-check-label mb-1" for="remember"
                                               style="display: inline-block;">
                                            {{ __('Remember Me') }}
                                        </label>
                                    </div>
                                    <div class="col-6 text-right forgot_pwd">
                                        <a href="{{ route('password.request') }}" class="btn btn-link">
                                            {{ __('Forgot password?') }}
                                        </a>
                                    </div>
                                </div>
                            </div>

                            {{--                            <div class="col-lg-12">--}}
                            {{--                                <a href="{{ route('register') }}"--}}
                            {{--                                   class="btn btn-success col-md-12 btn-block login_button">--}}
                            {{--                                    {{ __('Register a Restaurant') }}--}}
                            {{--                                </a>--}}
                            {{--                            </div>--}}


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

