@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'map'
])

@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card ">

                    <form>
                        {{--                    <form class="col-md-12" action="{{ route('profile.password') }}" method="POST">--}}
                        {{--                        @csrf--}}
                        {{--                        @method('PUT')--}}
                        <div class="card-header">
                            <h5 class="title">{{ __('Add Restaurant') }}</h5>
                        </div>
                        <div class="card-body">

                            <div class="row">
                                <label class="col-md-3 col-form-label">{{ __('Name') }}</label>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <input type="text" name="name" class="form-control"
                                               placeholder="Restaurant name" required>
                                    </div>
                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" style="display: block;" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="row">
                                <label class="col-md-3 col-form-label">{{ __('Contact Number') }}</label>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <input type="text" contact="name" class="form-control"
                                               placeholder="Contact Number" required>
                                    </div>
                                    @if ($errors->has('contact'))
                                        <span class="invalid-feedback" style="display: block;" role="alert">
                                            <strong>{{ $errors->first('contact') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-md-3 col-form-label">{{ __('Email') }}</label>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <input type="text" contact="email" class="form-control"
                                               placeholder="Email" required>
                                    </div>
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" style="display: block;" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                        </div>
                    </form>
                    <div class="card-body ">
                        <div id="map" class="map"></div>
                    </div>

                    <div class="row">
                        <label class="col-md-3 col-form-label">{{ __('Latitude') }}</label>
                        <div class="col-md-9">
                            <div class="form-group">
                                <input type="text" contact="latitude" class="form-control"
                                       placeholder="Latitude" required>
                            </div>
                            @if ($errors->has('latitude'))
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                            <strong>{{ $errors->first('latitude') }}</strong>
                                        </span>
                            @endif
                        </div>
                    </div>

                    <div class="row">
                        <label class="col-md-3 col-form-label">{{ __('Longitude') }}</label>
                        <div class="col-md-9">
                            <div class="form-group">
                                <input type="text" contact="longitude" class="form-control"
                                       placeholder="Longitude" required>
                            </div>
                            @if ($errors->has('latitude'))
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                            <strong>{{ $errors->first('longitude') }}</strong>
                                        </span>
                            @endif
                        </div>
                    </div>



                    <div class="card-footer ">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-info btn-round">{{ __('Submit') }}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function () {
            demo.initGoogleMaps();
        });
    </script>
@endpush
