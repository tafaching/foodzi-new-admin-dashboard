@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'tables'
])


@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <i class="nc-icon"><img src="{{ asset('paper/img/user-profile.png') }}"></i> User
                        <a href="{{ route('page.index', 'creates') }}" class="btn btn-success btn-sm float-right"><i
                                class="fas fa-plus"></i> Create User</a>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead class=" text-primary">
                                <tr>
                                    <th scope="col">ID</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Role</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Edit</th>
                                    <th scope="col">Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($allusers as $user)
                                    <tr>
                                        <td>{{$user->id}}</td>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->role}}</td>
                                        <td>{{$user->email}}</td>
                                        <td><a href="/pages/tables/{{$user->id}}/edit" class="btn btn-warning">Edit</a></td>
                                        <td>
                                            <form action="/pages/tables/{{$user->id}}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                <input type="submit" value="Delete" class="btn btn-danger">
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
