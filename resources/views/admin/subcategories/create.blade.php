@extends('layouts.dash', [
    'class' => '',
    'elementActive' => 'permission'
])
@section('content')
    <div class="content">
        <div class="card">
            <div class="card-header">
                {{ trans('global.create') }} {{ trans('cruds.subcategory.title_singular') }}
            </div>

            <div class="card-body">
                <form method="POST" action="{{ route("sub-store") }}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label class="required" for="name">{{ trans('cruds.subcategory.fields.name') }}</label>
                        <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text"
                               name="name" id="name" value="{{ old('name', '') }}" required>
                        @if($errors->has('name'))
                            <div class="invalid-feedback">
                                {{ $errors->first('name') }}
                            </div>
                        @endif
                        <span class="help-block">{{ trans('cruds.subcategory.fields.name_helper') }}</span>
                    </div>
                    <div class="form-group">
                        <label for="description">{{ 'Description(Optional)' }}</label>
                        <textarea class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}"
                                  name="description" id="description">{{ old('description') }}</textarea>
                        @if($errors->has('description'))
                            <div class="invalid-feedback">
                                {{ $errors->first('description') }}
                            </div>
                        @endif
                        <span class="help-block">{{ trans('cruds.shop.fields.description_helper') }}</span>
                    </div>
                    <div class="form-group">
                        <label for="price">{{ 'Price(Optional)' }}</label>
                        <input class="form-control {{ $errors->has('price') ? 'is-invalid' : '' }}" type="text"
                               name="price" id="price" value="{{ old('price', '') }}">
                        @if($errors->has('price'))
                            <div class="invalid-feedback">
                                {{ $errors->first('price') }}
                            </div>
                        @endif
                        <span class="help-block">{{ trans('cruds.shop.fields.name_helper') }}</span>
                    </div>
                    <div class="form-group">
                        <label for="photos">{{ 'Photos(Optional)' }}</label>
                        <div class="needsclick dropzone {{ $errors->has('photos') ? 'is-invalid' : '' }}"
                             id="photos-dropzone">
                        </div>
                        @if($errors->has('photos'))
                            <div class="invalid-feedback">
                                {{ $errors->first('photos') }}
                            </div>
                        @endif
                        <span class="help-block">{{ trans('cruds.shop.fields.photos_helper') }}</span>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="left-area">
                                <h7 class="heading">{{ __("Category") }}*</h7>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <select  name="category_id" required="">
                                <option value="">{{ __("Select Category") }}</option>
                                @foreach(\App\Category::all() as $cat)
                                    <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <button class="btn btn-danger" type="submit">
                            {{ trans('global.save') }}
                        </button>
                    </div>
                </form>


            </div>
        </div>
    </div>
@endsection
