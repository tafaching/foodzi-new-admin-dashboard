@extends('layouts.dash', [
    'class' => '',
    'elementActive' => 'permission'
])
@section('content')
    <div class="content">

        <div class="card">
            <div class="card-header">
                {{ trans('global.edit') }} {{ trans('cruds.category.title_singular') }}
            </div>

            <div class="card-body">
                <form method="POST" action="{{ route("sub-update", [$category->id]) }}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label class="required" for="name">{{ trans('cruds.category.fields.name') }}</label>
                        <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text"
                               name="name" id="name" value="{{ old('name', $category->name) }}" required>
                        @if($errors->has('name'))
                            <div class="invalid-feedback">
                                {{ $errors->first('name') }}
                            </div>
                        @endif
                        <span class="help-block">{{ trans('cruds.category.fields.name_helper') }}</span>
                    </div>
                    <div class="form-group">
                        <label for="description">{{ trans('cruds.shop.fields.description') }}</label>
                        <textarea class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}" name="description" id="description">{{ old('description', $category->description) }}</textarea>
                        @if($errors->has('description'))
                            <div class="invalid-feedback">
                                {{ $errors->first('description') }}
                            </div>
                        @endif
                        <span class="help-block">{{ trans('cruds.shop.fields.description_helper') }}</span>
                    </div>
                    <div class="form-group">
                        <label for="photos">{{ trans('cruds.shop.fields.photos') }}</label>
                        <div class="needsclick dropzone {{ $errors->has('photos') ? 'is-invalid' : '' }}" id="photos-dropzone">
                        </div>
                        @if($errors->has('photos'))
                            <div class="invalid-feedback">
                                {{ $errors->first('photos') }}
                            </div>
                        @endif
                        <span class="help-block">{{ trans('cruds.shop.fields.photos_helper') }}</span>
                    </div>

                    <div class="row">
                        <div class="col-lg-4">
                            <div class="left-area">
                                <h7 class="heading">{{ __("Category") }}*</h7>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <select name="category_id" required="">
                                <option value="">{{ __("Select Category") }}</option>
                                @foreach(\App\Category::all() as $cat)
                                    <option
                                        value="{{ $cat->id }}" {{ $category->category_id == $cat->id ? 'selected' :'' }}>{{ $cat->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <button class="btn btn-danger" type="submit">
                            {{ trans('global.save') }}
                        </button>
                    </div>
                </form>


            </div>
        </div>
    </div>
@endsection
