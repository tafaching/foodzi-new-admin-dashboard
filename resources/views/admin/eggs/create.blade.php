@extends('layouts.admin')



@section('content')
    <div class="content">
        <div class="card">
            <div class="card-header">
                {{ trans('global.create') }} {{ 'Eggs List' }}
            </div>

            <div class="card-body">
                <form method="POST" action="{{ route("eggs-store") }}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label class="required" for="name">{{ trans('cruds.shop.fields.name') }}</label>
                        <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text"
                               name="name" id="name" value="{{ old('name', '') }}" required>
                        @if($errors->has('name'))
                            <div class="invalid-feedback">
                                {{ $errors->first('name') }}
                            </div>
                        @endif
                        <span class="help-block">{{ trans('cruds.shop.fields.name_helper') }}</span>
                    </div>

                    <div class="form-group">
                        <label for="description">{{ 'Description(Optional)' }}</label>
                        <textarea class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}"
                                  name="description" id="description">{{ old('description') }}</textarea>
                        @if($errors->has('description'))
                            <div class="invalid-feedback">
                                {{ $errors->first('description') }}
                            </div>
                        @endif
                        <span class="help-block">{{ trans('cruds.shop.fields.description_helper') }}</span>
                    </div>
                    <div class="form-group">
                        <label for="price">{{ 'Price(Optional)' }}</label>
                        <input class="form-control {{ $errors->has('price') ? 'is-invalid' : '' }}" type="text"
                               name="price" id="price" value="{{ old('price', '') }}">
                        @if($errors->has('price'))
                            <div class="invalid-feedback">
                                {{ $errors->first('price') }}
                            </div>
                        @endif
                        <span class="help-block">{{ trans('cruds.shop.fields.name_helper') }}</span>
                    </div>
                    <div class="form-group">
                        <label for="photos">{{ 'Photos(Optional)' }}</label>
                        <div class="needsclick dropzone {{ $errors->has('photos') ? 'is-invalid' : '' }}"
                             id="photos-dropzone">
                        </div>
                        @if($errors->has('photos'))
                            <div class="invalid-feedback">
                                {{ $errors->first('photos') }}
                            </div>
                        @endif
                        <span class="help-block">{{ trans('cruds.shop.fields.photos_helper') }}</span>
                    </div>


                    <div class="form-group">
                        <button class="btn btn-danger" type="submit">
                            {{ trans('global.save') }}
                        </button>
                    </div>
                </form>



            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script
        src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_API_KEY') }}&libraries=places&callback=initialize&language=en&region=GB"
        async defer></script>
    <script src="/js/mapInput.js"></script>
{{--    <script>--}}
{{--        var uploadedPhotosMap = {}--}}
{{--        Dropzone.options.photosDropzone = {--}}
{{--            url: '{{ route('admin.bacon.storeMedia') }}',--}}
{{--            maxFilesize: 2, // MB--}}
{{--            acceptedFiles: '.jpeg,.jpg,.png,.gif',--}}
{{--            addRemoveLinks: true,--}}
{{--            headers: {--}}
{{--                'X-CSRF-TOKEN': "{{ csrf_token() }}"--}}
{{--            },--}}
{{--            params: {--}}
{{--                size: 2,--}}
{{--                width: 4096,--}}
{{--                height: 4096--}}
{{--            },--}}
{{--            success: function (file, response) {--}}
{{--                $('form').append('<input type="hidden" name="photos[]" value="' + response.name + '">')--}}
{{--                uploadedPhotosMap[file.name] = response.name--}}
{{--            },--}}
{{--            removedfile: function (file) {--}}
{{--                console.log(file)--}}
{{--                file.previewElement.remove()--}}
{{--                var name = ''--}}
{{--                if (typeof file.file_name !== 'undefined') {--}}
{{--                    name = file.file_name--}}
{{--                } else {--}}
{{--                    name = uploadedPhotosMap[file.name]--}}
{{--                }--}}
{{--                $('form').find('input[name="photos[]"][value="' + name + '"]').remove()--}}
{{--            },--}}
{{--            init: function () {--}}
{{--                @if(isset($bacon) && $bacon->photos)--}}
{{--                var files =--}}
{{--                {!! json_encode($bacon->photos) !!}--}}
{{--                    for (var i in files) {--}}
{{--                    var file = files[i]--}}
{{--                    this.options.addedfile.call(this, file)--}}
{{--                    this.options.thumbnail.call(this, file, file.url)--}}
{{--                    file.previewElement.classList.add('dz-complete')--}}
{{--                    $('form').append('<input type="hidden" name="photos[]" value="' + file.file_name + '">')--}}
{{--                }--}}
{{--                @endif--}}
{{--            },--}}
{{--            error: function (file, response) {--}}
{{--                if ($.type(response) === 'string') {--}}
{{--                    var message = response //dropzone sends it's own error messages in string--}}
{{--                } else {--}}
{{--                    var message = response.errors.file--}}
{{--                }--}}
{{--                file.previewElement.classList.add('dz-error')--}}
{{--                _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')--}}
{{--                _results = []--}}
{{--                for (_i = 0, _len = _ref.length; _i < _len; _i++) {--}}
{{--                    node = _ref[_i]--}}
{{--                    _results.push(node.textContent = message)--}}
{{--                }--}}
{{--                return _results--}}
{{--            }--}}
{{--        }--}}
{{--    </script>--}}
@endsection
