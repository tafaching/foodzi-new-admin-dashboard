@extends('layouts.dash', [
    'class' => '',
    'elementActive' => 'permission'
])
@section('content')
    <div class="content">
        <div class="card">
            <div class="card-header">
                {{ trans('global.create') }} {{ trans('cruds.childCategory.title_singular') }}
            </div>

            <div class="card-body">
                <form method="POST" action="{{ route("child-store") }}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label class="required" for="name">{{ trans('cruds.subcategory.fields.name') }}</label>
                        <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text"
                               name="name" id="name" value="{{ old('name', '') }}" required>
                        @if($errors->has('name'))
                            <div class="invalid-feedback">
                                {{ $errors->first('name') }}
                            </div>
                        @endif
                        <span class="help-block">{{ trans('cruds.subcategory.fields.name_helper') }}</span>
                    </div>

                    <div class="row">
                        <div class="col-lg-4">
                            <div class="left-area">
                                <h7 class="heading">{{ __("Category") }}*</h7>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <select name="sub_category_id" required="">
                                <option value="">{{ __("Select Category") }}</option>
                                @foreach(\App\Category::all() as $cat)
                                    <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <button class="btn btn-danger" type="submit">
                            {{ trans('global.save') }}
                        </button>
                    </div>



                    {{--Form roll for options--}}
                    <div class="card">
                        <div class="card-header">
                            {{ trans('cruds.subcategory.title_singular') }} {{ trans('global.list') }}
                        </div>

                        <div class="card-body">
                            <div class="table-responsive">
                                <table class=" table table-bordered table-striped table-hover datatable datatable-Category">
                                    <thead>
                                    <tr>

                                        <th>
                                            {{ trans('cruds.category.fields.name') }}
                                        </th>

                                        <th>
                                            {{ 'Description' }}
                                        </th>

                                        <th>
                                            {{ 'Select' }}
                                        </th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach(\App\Subcategory::all() as $key => $subcategory)
                                        <tr data-entry-id="{{ $subcategory->id }}">


                                            <td>
                                                {{ $subcategory->name ?? '' }}
                                            </td>
                                            <td>
                                                {{ $subcategory->description ?? '' }}
                                            </td>

                                            <td>
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="">
                                                        <span class="form-check-sign">
                                                    <span class="check"></span>
                                                    </span>
                                                    </label>
                                                </div>
                                            </td>


                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>




            </div>
        </div>
    </div>
                </form>


@endsection
