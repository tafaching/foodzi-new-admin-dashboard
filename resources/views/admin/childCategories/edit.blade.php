@extends('layouts.dash', [
    'class' => '',
    'elementActive' => 'permission'
])
@section('content')
    <div class="content">

        <div class="card">
            <div class="card-header">
                {{ trans('global.edit') }} {{ trans('cruds.childCategory.title_singular') }}
            </div>

            <div class="card-body">
                <form method="POST" action="{{ route("child-update", [$category->id]) }}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label class="required" for="name">{{ trans('cruds.category.fields.name') }}</label>
                        <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text"
                               name="name" id="name" value="{{ old('name', $category->name) }}" required>
                        @if($errors->has('name'))
                            <div class="invalid-feedback">
                                {{ $errors->first('name') }}
                            </div>
                        @endif
                        <span class="help-block">{{ trans('cruds.category.fields.name_helper') }}</span>
                    </div>

                    <div class="row">
                        <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __("Category") }}*</h4>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <select name="sub_category_id" required="">
                                <option value="">{{ __("Select Category") }}</option>
                                @foreach(\App\Subcategory::all() as $cat)
                                    <option
                                        value="{{ $cat->id }}" {{ $category->sub_category_id == $cat->id ? 'selected' :'' }}>{{ $cat->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <button class="btn btn-danger" type="submit">
                            {{ trans('global.save') }}
                        </button>
                    </div>
                </form>


            </div>
        </div>
    </div>
@endsection
