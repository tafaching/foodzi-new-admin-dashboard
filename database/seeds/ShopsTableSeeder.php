<?php

use App\Category;
use App\Role;
use Illuminate\Database\Seeder;

class ShopsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $pictures = collect(range(1,11));
        $users = Role::findOrFail(2)->users;
        $categories = Category::all()->pluck('id');

        $addresses = [
            [
                "address" => "Olievenhout Ave, Shop B1, Northview Centre, Northgate",
                "latitude" => "-26.2309",
                "longitude" => "28.0583"
            ],
            [
                "address" => "Olievenhout Ave, Shop B1, Northview Centre, Northgate",
                "latitude" => "-26.2309",
                "longitude" => "28.0583"
            ],
            [
                "address" => "Sports Direct, Oxford Street, Rosebank",
                "latitude" => "-26.2309",
                "longitude" => "28.0583"
            ],

        ];
        $currentAddress = 0;

        foreach($users as $user)
        {
            $shop = [
                'name' => $faker->company,
                'description' => $faker->paragraph,
                'address' => $faker->address,
                'active' => 1,
            ];
            $shop = $user->shops()->create(array_merge($shop, $addresses[$currentAddress++]));
            $shop->categories()->sync($categories->random(rand(0,2)));

            foreach($pictures->random(rand(1,2)) as $index)
            {
                $shop->addMedia(public_path("assets/images/shops/a$index.jpg"))->preservingOriginal()->toMediaCollection('photos');
            }
        }
    }
}
