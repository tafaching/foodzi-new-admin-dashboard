<?php

//RUN THIS IN PRODUCTION TO CREATE A STORAGE LINK  www.example.com/linkstorage
Route::get('/linkstorage', function () {
    Artisan::call('storage:link');
});


Route::get('/home', function () {
    if (session('status')) {
        return redirect()->route('admin.shops.index')->with('status', session('status'));
    }

    return redirect()->route('admin.shops.index');
});
Route::get('/', function () {
    return view('auth.login');
});
//Route::get('/', 'HomeController@index')->name('home');
Route::get('shop/{shop}', 'HomeController@show')->name('shop');

Auth::routes();

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    // Permissions
    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');
    Route::resource('permissions', 'PermissionsController');

    // Roles
    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');
    Route::resource('roles', 'RolesController');

    // Users
    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');
    Route::resource('users', 'UsersController');

    // Categories
    Route::delete('categories/destroy', 'CategoriesController@massDestroy')->name('categories.massDestroy');
    Route::resource('categories', 'CategoriesController');

    // Shops
    Route::delete('shops/destroy', 'ShopsController@massDestroy')->name('shops.massDestroy');
    Route::post('shops/media', 'ShopsController@storeMedia')->name('shops.storeMedia');
    Route::resource('shops', 'ShopsController');


});


Route::get('page', ['as' => 'page.index',  'uses' => 'DashboardController@index']);

// ADMIN CATEGORY

Route::group(['middleware'=>'permissions:categories'],function (){
   Route::get('/category','Admin\CategoryController@index')->name('admin-cat-index');
});

//SUBCATEGORIES
Route::get('/sub-index', 'Admin\SubCategoriesController@subIndex')->name('sub-index');
Route::get('/sub-create', 'Admin\SubCategoriesController@subCreate')->name('sub-create');
Route::get('show-sub/{category}', 'Admin\SubCategoriesController@subShow')->name('sub-show');
Route::get('edit-sub/{category}', 'Admin\SubCategoriesController@subEdit')->name('sub-edit');
Route::delete('delete-sub/{category}', 'Admin\SubCategoriesController@subDestroy')->name('sub-delete');
Route::post('sub-store', 'Admin\SubCategoriesController@subStore')->name('sub-store');
Route::post('update-sub/{category}', 'Admin\SubCategoriesController@subUpdate')->name('sub-update');
Route::delete('categories/destroy', 'Admin\SubCategoriesController@massDestroy')->name('sub-massDestroy');

Route::post('/subcategory/edit/{id}', 'Admin\SubCategoriesController@update')->name('admin-subcat-update');

//CHILD CATEGORIES
Route::get('/child-index', 'Admin\ChildCategoryController@index')->name('child-index');
Route::get('/child-create', 'Admin\ChildCategoryController@create')->name('child-create');
Route::get('show/{category}', 'Admin\ChildCategoryController@show')->name('child-show');
Route::get('edit/{category}', 'Admin\ChildCategoryController@edit')->name('child-edit');
Route::delete('delete/{category}', 'Admin\ChildCategoryController@destroy')->name('child-delete');
Route::post('store', 'Admin\ChildCategoryController@store')->name('child-store');
Route::post('update/{category}', 'Admin\ChildCategoryController@update')->name('child-update');

Route::get('/bacon-index', 'Admin\BaconController@index')->name('bacon-index');
Route::get('/bacon-create', 'Admin\BaconController@create')->name('bacon-create');
Route::get('show-bacon/{bacon}', 'Admin\BaconController@show')->name('bacon-show');
Route::get('edit-bacon/{bacon}', 'Admin\BaconController@edit')->name('bacon-edit');
Route::delete('delete-bacon/{bacon}', 'Admin\BaconController@destroy')->name('bacon-delete');
Route::post('bacon-store', 'Admin\BaconController@store')->name('bacon-store');
Route::post('update-bac/{bacon}', 'Admin\BaconController@update')->name('bacon-update');

Route::get('/extra-index', 'Admin\BreakfastExtraController@index')->name('extra-index');
Route::get('/extra-create', 'Admin\BreakfastExtraController@create')->name('extra-create');
Route::get('show-extra/{extra}', 'Admin\BreakfastExtraController@show')->name('extra-show');
Route::get('edit-extra/{extra}', 'Admin\BreakfastExtraController@edit')->name('extra-edit');
Route::delete('delete-extra/{extra}', 'Admin\BreakfastExtraController@destroy')->name('extra-delete');
Route::post('extra-store', 'Admin\BreakfastExtraController@store')->name('extra-store');
Route::post('update-extra/{extra}', 'Admin\BreakfastExtraController@update')->name('extra-update');


Route::get('/eggs-index', 'Admin\EggsController@index')->name('eggs-index');
Route::get('/eggs-create', 'Admin\EggsController@create')->name('eggs-create');
Route::get('show-eggs/{eggs}', 'Admin\EggsController@show')->name('eggs-show');
Route::get('edit-eggs/{eggs}', 'Admin\EggsController@edit')->name('eggs-edit');
Route::delete('delete-eggs/{eggs}', 'Admin\EggsController@destroy')->name('eggs-delete');
Route::post('eggs-store', 'Admin\EggsController@store')->name('eggs-store');
Route::post('update-eggs/{eggs}', 'Admin\EggsController@update')->name('eggs-update');

Route::get('/coffee-index', 'Admin\CoffeeController@index')->name('coffee-index');
Route::get('/coffee-create', 'Admin\CoffeeController@create')->name('coffee-create');
Route::get('show-coffee/{coffee}', 'Admin\CoffeeController@show')->name('coffee-show');
Route::get('edit-coffee/{coffee}', 'Admin\CoffeeController@edit')->name('coffee-edit');
Route::delete('delete-coffee/{coffee}', 'Admin\CoffeeController@destroy')->name('coffee-delete');
Route::post('coffee-store', 'Admin\CoffeeController@store')->name('coffee-store');
Route::post('update-coffee/{coffee}', 'Admin\CoffeeController@update')->name('coffee-update');
