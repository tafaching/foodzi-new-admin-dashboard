<?php

Route::group(['prefix' => 'v1', 'as' => 'api.', 'namespace' => 'Api\V1\Admin', 'middleware' => ['auth:api']], function () {
    // Permissions
    Route::apiResource('permissions', 'PermissionsApiController');

    // Roles
    Route::apiResource('roles', 'RolesApiController');

    // Users
    Route::apiResource('users', 'UsersApiController');

    // Categories
    Route::apiResource('categories', 'CategoriesApiController');

    // Shops
    Route::post('shops/media', 'ShopsApiController@storeMedia')->name('shops.storeMedia');
    Route::apiResource('shops', 'ShopsApiController');

    // Bacon
    Route::post('bacon/media', 'BaconApiController@storeMedia')->name('bacon.storeMedia');
    Route::apiResource('bacon', 'BaconApiController');
});
